export default {
    namespaced : true,
    state: {
        transactions : 0,
    },
    mutations: {
        insert: (state, payload) => {
            state.transactions++
        }
    },
    action: {

    },
    getters: {
        transactions : state => state.transactions
    }
};