<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// // Route::get('/test', 'MiddleController@test')->middleware('dateMiddleware');

// Route::middleware('dateMiddleware')->group(function(){

//     // Route::get('/test', 'MiddleController@test');
// });

// Route::get('/test', 'MiddleController@test');

// Route::middleware(['auth','admin'])->group(function(){

//     Route::get('/admin', 'MiddleController@admin');
// });

// Route::middleware(['auth','verify'])->group(function(){

//     Route::get('/verify', 'MiddleController@verify');
//     Route::get('/home', 'HomeController@index')->name('home');
//    /*	Route::get('/', function () {
//         return view('welcome');
//     });*/
// });
// Route::get('/login', 'Auth\LoginController');
// // Route::get('/', function(){
// // 	return view('app');
// // });

Auth::routes();

Route::view('/{any?}', 'app')->where('any', '.*');






