 <?php

 use Illuminate\Http\Request;

 Route::group([
     'middleware'	 => 'api',
     'prefix'	 	 => 'auth', 
     'namespace'	 => 'Auth'
    ], function () {
     Route::post('login', 'LoginController');
     Route::post('register', 'RegisterController');
     Route::post('generate-otp', 'OtpController');
     Route::post('verifikasi', 'VerificationController');

     Route::get('/social/{provider}', 'SocialController@redirectToProvider');
     Route::get('/social/{provider}/callback', 'SocialController@handleProviderCallback');
 });
 Route::group([
    'middleware' => 'api',
    'prefix'     => 'campaigns',
], function(){
    Route::get('home', 'CampaignsController@index');
    Route::get('random/{count}', 'CampaignsController@random');
    Route::post('create', 'CampaignsController@store');

}); 
// Route::get('profiles/{profile}', 'ProfileController');
// Route::get('user', 'UserController');

Route::group([
    'prefix' => 'campaign',
    'middleware' => 'api'
], function () {
    //Route::get('random/{count}', 'CampaignsController@random');
    Route::post('store', 'CampaignsController@store');
    Route::get('/' , 'CampaignsController@index');
    Route::get('/{id}', 'CampaignsController@detail');
    Route::get('/search/{keyword}', 'CampaignsController@search');
});

Route::group([
    'prefix' => 'blog',
    'middleware' => 'api'
], function () {
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
});