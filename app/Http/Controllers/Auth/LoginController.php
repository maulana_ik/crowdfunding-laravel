<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\Auth\LoginRequest;
class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(LoginRequest $request)
    {
        
        $user = User::where('email', $request->email)->first();

        if (!$token = auth()->attempt($request->only('email', 'password'))) {
            return response()->json([
                'error' => 'Email password wrong'
            ],401);
        }elseif ($user->user_verified_at == null) {
            return response()->json([
                'error' => 'email not veified'
            ],401);
            # code...
        }else{
            $data['user'] = auth()->user();
            return response()->json([
                'response_code' => '00',
                'response_message' => 'success',
                'data' => $data
            ]);
        }

        // $request->validate([

        //     'email' => 'required',
        //     'password' => 'required'

        // ]);

        
        // if (!$token = auth()->attempt($request->only('email','password'))) {
        //     return response(null, 401);
        // }
        
        // return response()->json(compact('token'));




    }
}
