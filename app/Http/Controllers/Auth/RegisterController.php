<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;

use App\User;
use App\Otp;
use Carbon\Carbon;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        $request->validate([
            'email' => 'required|unique:users,email|email',
            'name' => 'required'
        ]);

        $data_request = $request->all();
        $user = User::create($data_request);
        $data['user'] = $user;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'User baru berhasil didaftarkan, Silahkan cek email untuk melihat kode otp',
            'data' => $data
        ]);






        /*User::find(1)->otp;
        
        $user = new User(['
        name' => request ('name'),
        'email' => request ('email'),
        'password' => bcrypt( request ('password')),
        'otp' => request (rand(100000,999999)),
        'valid_until' => request (Carbon::now())]);
        $user->otp()->save($otp);

        return response()->json([
            
            'response_code' => '00',
            'response_message' => 'Silahkan cek Email',
            'data' => $data
        ], 200);*/

    }
}
