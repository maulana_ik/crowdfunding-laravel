<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;

class AuthSocialiteController extends Controller
{
    public function redirectToProvider($provider)
    {
		$url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
		return response()->json([
			'url' => $url
		])
    },

    public function handleProviderCallback($provider){
    	try {
    		$social_user = Socialite::driver($provider)->stateless()->user();

    		dd($social_user);

    		if (!$social_user) {
    			# code...
    			return response()->json([
    				'response_code' => '01',
    				'response_message' => 'login failed'
    			])
    		}

    		$user = User::whereEmail($social_user->email)->first();

    		if(!$user){
    			if($provider == 'google'){
    				$photo_profle = $social_user->avatar;
    			}
    			$user = User::create([
    				'email' => $social_user->email,
    				'name'=> $social_user->name,
    				'email_verified_at' => Carbon::now(),
    				'photo_profle' => $photo_profle
    			]);
    		}
    		$data['user'] = $user,
    		$data['token'] = auth()->login($user);

    		return response()->json([
    			'response_code' => '00',
    			'response_message' => 'user login success',
    			'data' => $data
    		]);

    	} catch (\Throwable $th){
    		return response()->json([
    			'response_code' => '01',
    			'response_message' => 'user login failed',
    			
    		], 401);
    	}
    }
}
